From 76376a85ff5cbe555db6a6d729fcb83f56988f25 Mon Sep 17 00:00:00 2001
From: "Michael S. Tsirkin" <mst@redhat.com>
Date: Wed, 6 Nov 2024 17:29:35 -0500
Subject: [PATCH 7/7] virtio-net: disable USO for all RHEL9

RH-Author: MST <mst@redhat.com>
RH-MergeRequest: 319: virtio-net migration: ensure compatibility of rhel9*
RH-Jira: RHEL-69500
RH-Acked-by: Peter Xu <peterx@redhat.com>
RH-Acked-by: Miroslav Rezanina <mrezanin@redhat.com>
RH-Commit: [1/1] c7518d8293e1cfdd928f0ce454249ecb7bd7117a (mstredhat/qemu-kvm-centos)

With commit 298dae99b77f ("virtio-net: disable USO for RHEL9")
we fixed migration for 9.6 but we forgot to apply the fix
for 9.6 when it was created.

To help us not forget in 9.7 and beyond, create a generic RHEL9 compat
and apply it.

Upstream status: n/a: upstream has no guarantee if kernel features change
Tested: lightly on developer's machine.
JIRA: https://issues.redhat.com/browse/RHEL-69500
Signed-off-by: Michael S. Tsirkin <mst@redhat.com>

Patch-name: kvm-virtio-net-disable-USO-for-RHEL9.patch
Patch-id: 83
Patch-present-in-specfile: True
---
 hw/arm/virt.c              |  3 +++
 hw/core/machine.c          | 13 ++++++++-----
 hw/i386/pc_q35.c           |  3 +++
 hw/s390x/s390-virtio-ccw.c |  3 +++
 include/hw/boards.h        |  3 +++
 5 files changed, 20 insertions(+), 5 deletions(-)

diff --git a/hw/arm/virt.c b/hw/arm/virt.c
index 078098ec3a..6d55bba241 100644
--- a/hw/arm/virt.c
+++ b/hw/arm/virt.c
@@ -3595,6 +3595,9 @@ DEFINE_VIRT_MACHINE_AS_LATEST(10, 0, 0)
 static void virt_rhel_machine_9_6_0_options(MachineClass *mc)
 {
     virt_rhel_machine_10_0_0_options(mc);
+
+    /* NB: remember to move this line to the *latest* RHEL 9 machine */
+    compat_props_add(mc->compat_props, hw_compat_rhel_9, hw_compat_rhel_9_len);
 }
 DEFINE_VIRT_MACHINE(9, 6, 0)
 
diff --git a/hw/core/machine.c b/hw/core/machine.c
index 04d180eac4..1ee0e1dee8 100644
--- a/hw/core/machine.c
+++ b/hw/core/machine.c
@@ -327,6 +327,14 @@ GlobalProperty hw_compat_rhel_10_0[] = {
 };
 const size_t hw_compat_rhel_10_0_len = G_N_ELEMENTS(hw_compat_rhel_10_0);
 
+/* Apply this to all RHEL9 boards going backward and forward */
+GlobalProperty hw_compat_rhel_9[] = {
+    /* supported by userspace, but RHEL 9 *kernels* do not support USO. */
+    { TYPE_VIRTIO_NET, "host_uso", "off"},
+    { TYPE_VIRTIO_NET, "guest_uso4", "off"},
+    { TYPE_VIRTIO_NET, "guest_uso6", "off"},
+};
+const size_t hw_compat_rhel_9_len = G_N_ELEMENTS(hw_compat_rhel_9);
 
 GlobalProperty hw_compat_rhel_9_5[] = {
   /* hw_compat_rhel_9_5 from hw_compat_8_2 */
@@ -337,11 +345,6 @@ GlobalProperty hw_compat_rhel_9_5[] = {
     { TYPE_VIRTIO_IOMMU_PCI, "aw-bits", "64" },
   /* hw_compat_rhel_9_5 from hw_compat_8_2 */
     { "virtio-gpu-device", "x-scanout-vmstate-version", "1" },
-    /* supported by userspace, but RHEL 9 *kernels* do not support USO. */
-    /* TODO: if we ever add 9.6 compat, this has to be there, too */
-    { TYPE_VIRTIO_NET, "host_uso", "off"},
-    { TYPE_VIRTIO_NET, "guest_uso4", "off"},
-    { TYPE_VIRTIO_NET, "guest_uso6", "off"},
 };
 const size_t hw_compat_rhel_9_5_len = G_N_ELEMENTS(hw_compat_rhel_9_5);
 
diff --git a/hw/i386/pc_q35.c b/hw/i386/pc_q35.c
index 506f9dc0c0..b2b8124225 100644
--- a/hw/i386/pc_q35.c
+++ b/hw/i386/pc_q35.c
@@ -690,6 +690,9 @@ static void pc_q35_rhel_machine_9_6_0_options(MachineClass *m)
     m->alias = NULL;
     pcmc->smbios_stream_product = "RHEL";
     pcmc->smbios_stream_version = "9.6.0";
+
+    /* NB: remember to move this line to the *latest* RHEL 9 machine */
+    compat_props_add(m->compat_props, hw_compat_rhel_9, hw_compat_rhel_9_len);
 }
 
 DEFINE_Q35_MACHINE_BUGFIX(9, 6, 0);
diff --git a/hw/s390x/s390-virtio-ccw.c b/hw/s390x/s390-virtio-ccw.c
index 0347dc69ca..2e276ad72b 100644
--- a/hw/s390x/s390-virtio-ccw.c
+++ b/hw/s390x/s390-virtio-ccw.c
@@ -1305,6 +1305,9 @@ static void ccw_rhel_machine_9_6_0_instance_options(MachineState *machine)
 static void ccw_rhel_machine_9_6_0_class_options(MachineClass *mc)
 {
     ccw_rhel_machine_10_0_0_class_options(mc);
+
+    /* NB: remember to move this line to the *latest* RHEL 9 machine */
+    compat_props_add(mc->compat_props, hw_compat_rhel_9, hw_compat_rhel_9_len);
 }
 DEFINE_CCW_MACHINE(9, 6, 0);
 
diff --git a/include/hw/boards.h b/include/hw/boards.h
index ac917b87fb..83d11de42f 100644
--- a/include/hw/boards.h
+++ b/include/hw/boards.h
@@ -805,6 +805,9 @@ extern const size_t hw_compat_2_1_len;
 extern GlobalProperty hw_compat_rhel_10_0[];
 extern const size_t hw_compat_rhel_10_0_len;
 
+extern GlobalProperty hw_compat_rhel_9[];
+extern const size_t hw_compat_rhel_9_len;
+
 extern GlobalProperty hw_compat_rhel_9_5[];
 extern const size_t hw_compat_rhel_9_5_len;
 
-- 
2.39.3

