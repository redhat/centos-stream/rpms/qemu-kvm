From 728e3d8a124f4ec51c005ad6867270f3e60df16c Mon Sep 17 00:00:00 2001
From: Kevin Wolf <kwolf@redhat.com>
Date: Fri, 20 Aug 2021 18:25:12 +0200
Subject: qcow2: Deprecation warning when opening v2 images rw

qcow2 v3 has been around for a long time (since QEMU 1.1/RHEL 7), so
there is no real reason any more to use it. People still using it might
do so unintentionally. Warn about it and suggest upgrading during the
RHEL 9 timeframe so that the code can possibly be disabled in RHEL 10.

The warning is restricted to read-write mode and the system emulator.
The primary motivation for not having it in qemu-img is that 'qemu-img
amend' for upgrades would warn otherwise. It also avoids having to make
too many changes to the test suite.

bdrv_uses_whitelist() is used as a proxy for deciding whether we are
running in a tool or the system emulator. This is not entirely clean,
but it's what is available and the same function qcow2_do_open() already
uses it this way for another warning.

Signed-off-by: Kevin Wolf <kwolf@redhat.com>
---
 block/qcow2.c                    | 6 ++++++
 tests/qemu-iotests/common.filter | 1 +
 2 files changed, 7 insertions(+)

diff --git a/block/qcow2.c b/block/qcow2.c
index 70b19730a3..a4cffb628c 100644
--- a/block/qcow2.c
+++ b/block/qcow2.c
@@ -1358,6 +1358,12 @@ qcow2_do_open(BlockDriverState *bs, QDict *options, int flags,
         ret = -ENOTSUP;
         goto fail;
     }
+    if (header.version < 3 && !bdrv_is_read_only(bs) && bdrv_uses_whitelist()) {
+        warn_report_once("qcow2 v2 images are deprecated and may not be "
+                         "supported in future versions. Please consider "
+                         "upgrading the image with 'qemu-img amend "
+                         "-o compat=v3'.");
+    }
 
     s->qcow_version = header.version;
 
diff --git a/tests/qemu-iotests/common.filter b/tests/qemu-iotests/common.filter
index fc3c64bcb8..4b238954d5 100644
--- a/tests/qemu-iotests/common.filter
+++ b/tests/qemu-iotests/common.filter
@@ -83,6 +83,7 @@ _filter_qemu()
 {
     gsed -e "s#\\(^\\|(qemu) \\)$(basename $QEMU_PROG):#\1QEMU_PROG:#" \
         -e 's#^QEMU [0-9]\+\.[0-9]\+\.[0-9]\+ monitor#QEMU X.Y.Z monitor#' \
+        -e "/qcow2 v2 images are deprecated/d" \
         -e $'s#\r##' # QEMU monitor uses \r\n line endings
 }
 
-- 
2.39.3

