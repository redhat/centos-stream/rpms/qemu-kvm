From dcfaf859fa755f9c75b1e848f1b61b293b500d0d Mon Sep 17 00:00:00 2001
From: Hendrik Brueckner <brueckner@linux.ibm.com>
Date: Fri, 6 Dec 2024 13:27:48 +0100
Subject: [PATCH 13/18] s390x/cpumodel: Add Sequential-Instruction-Fetching
 facility
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

RH-Author: Thomas Huth <thuth@redhat.com>
RH-MergeRequest: 305: CPU model for new IBM Z gen17 hardware
RH-Jira: RHEL-32665
RH-Acked-by: Cornelia Huck <cohuck@redhat.com>
RH-Acked-by: Cédric Le Goater <clg@redhat.com>
RH-Commit: [13/16] 5f8facceda9ecad3b417ed3da2d360fdd480f6d4 (thuth/qemu-kvm-cs9)

The sequential instruction fetching facility provides few guarantees,
for example, to avoid stop machine calls on enabling/disabling kprobes.

Signed-off-by: Hendrik Brueckner <brueckner@linux.ibm.com>
Reviewed-by: Janosch Frank <frankja@linux.ibm.com>
Message-ID: <20241206122751.189721-13-brueckner@linux.ibm.com>
Signed-off-by: Thomas Huth <thuth@redhat.com>
(cherry picked from commit a5fa8bee72847406bfc32e15c8e41c0a2a0812e1)
---
 target/s390x/cpu_features_def.h.inc | 1 +
 1 file changed, 1 insertion(+)

diff --git a/target/s390x/cpu_features_def.h.inc b/target/s390x/cpu_features_def.h.inc
index 2c1d1cd98a..09a80844a7 100644
--- a/target/s390x/cpu_features_def.h.inc
+++ b/target/s390x/cpu_features_def.h.inc
@@ -91,6 +91,7 @@ DEF_FEAT(DFP_PACKED_CONVERSION, "dfppc", STFL, 80, "Decimal-floating-point packe
 DEF_FEAT(PPA15, "ppa15", STFL, 81, "PPA15 is installed")
 DEF_FEAT(BPB, "bpb", STFL, 82, "Branch prediction blocking")
 DEF_FEAT(MISC_INSTRUCTION_EXT4, "minste4", STFL, 84, "Miscellaneous-Instruction-Extensions Facility 4")
+DEF_FEAT(SIF, "sif", STFL, 85, "Sequential-instruction-fetching facility")
 DEF_FEAT(MSA_EXT_12, "msa12-base", STFL, 86, "Message-security-assist-extension-12 facility (excluding subfunctions)")
 DEF_FEAT(VECTOR, "vx", STFL, 129, "Vector facility")
 DEF_FEAT(INSTRUCTION_EXEC_PROT, "iep", STFL, 130, "Instruction-execution-protection facility")
-- 
2.39.3

