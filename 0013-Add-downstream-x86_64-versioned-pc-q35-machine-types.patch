From c8510c21f0fde361d6cbce81bfb2f4acb6941b58 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Daniel=20P=2E=20Berrang=C3=A9?= <berrange@redhat.com>
Date: Wed, 3 Jul 2024 13:44:41 +0100
Subject: Add downstream x86_64 versioned 'pc' & 'q35' machine types

Adding changes to add RHEL machine types for x86_64 architecture.

Signed-off-by: Miroslav Rezanina <mrezanin@redhat.com>
---
Rebase notes (9.1.0 rc0):
- Merged pc_q35_machine_rhel_options back into
  pc_q35_machine_options to reduce delta to upstream
- Convert to new DEFINE_(I440FX|Q35)_MACHINE macros

Rebase notes (9.1.0 rc4):
- Moved x86 cpu deprecation note to device disable patch

Merged patches (9.1.0 rc0):
- 043ad5ce97 Add upstream compatibility bits (partial)
---
 hw/i386/fw_cfg.c           |   2 +-
 hw/i386/pc.c               | 159 ++++++++++++++++++++++++++++-
 hw/i386/pc_piix.c          | 102 ++++++++++++++++++-
 hw/i386/pc_q35.c           | 204 +++++++++++++++++++++++++++++++++++--
 include/hw/boards.h        |   2 +
 include/hw/i386/pc.h       |  33 ++++++
 target/i386/kvm/kvm-cpu.c  |   1 +
 target/i386/kvm/kvm.c      |   4 +
 tests/qtest/pvpanic-test.c |   5 +-
 9 files changed, 499 insertions(+), 13 deletions(-)

diff --git a/hw/i386/fw_cfg.c b/hw/i386/fw_cfg.c
index 33ef280420..a322709ffa 100644
--- a/hw/i386/fw_cfg.c
+++ b/hw/i386/fw_cfg.c
@@ -73,7 +73,7 @@ void fw_cfg_build_smbios(PCMachineState *pcms, FWCfgState *fw_cfg,
 
     if (pcmc->smbios_defaults) {
         /* These values are guest ABI, do not change */
-        smbios_set_defaults("QEMU", mc->desc, mc->name,
+        smbios_set_defaults("Red Hat", "KVM", mc->desc,
                             pcmc->smbios_stream_product, pcmc->smbios_stream_version);
     }
 
diff --git a/hw/i386/pc.c b/hw/i386/pc.c
index 7779c88a91..a49d346d2e 100644
--- a/hw/i386/pc.c
+++ b/hw/i386/pc.c
@@ -276,6 +276,161 @@ const size_t pc_compat_2_4_len = G_N_ELEMENTS(pc_compat_2_4);
  */
 #define PC_FW_DATA (0x20000 + 0x8000)
 
+/* This macro is for changes to properties that are RHEL specific,
+ * different to the current upstream and to be applied to the latest
+ * machine type.
+ */
+GlobalProperty pc_rhel_compat[] = {
+    /* we don't support s3/s4 suspend */
+    { "PIIX4_PM", "disable_s3", "1" },
+    { "PIIX4_PM", "disable_s4", "1" },
+    { "ICH9-LPC", "disable_s3", "1" },
+    { "ICH9-LPC", "disable_s4", "1" },
+
+    { TYPE_X86_CPU, "host-phys-bits", "on" },
+    { TYPE_X86_CPU, "host-phys-bits-limit", "48" },
+    { TYPE_X86_CPU, "vmx-entry-load-perf-global-ctrl", "off" },
+    { TYPE_X86_CPU, "vmx-exit-load-perf-global-ctrl", "off" },
+    /* bz 1508330 */
+    { "vfio-pci", "x-no-geforce-quirks", "on" },
+    /* bz 1941397 */
+    { TYPE_X86_CPU, "kvm-asyncpf-int", "on" },
+};
+const size_t pc_rhel_compat_len = G_N_ELEMENTS(pc_rhel_compat);
+
+GlobalProperty pc_rhel_9_3_compat[] = {
+    /* pc_rhel_9_3_compat from pc_compat_8_0 */
+    { "virtio-mem", "unplugged-inaccessible", "auto" },
+};
+const size_t pc_rhel_9_3_compat_len = G_N_ELEMENTS(pc_rhel_9_3_compat);
+
+GlobalProperty pc_rhel_9_2_compat[] = {
+    /* pc_rhel_9_2_compat from pc_compat_7_2 */
+    { "ICH9-LPC", "noreboot", "true" },
+};
+const size_t pc_rhel_9_2_compat_len = G_N_ELEMENTS(pc_rhel_9_2_compat);
+
+GlobalProperty pc_rhel_9_0_compat[] = {
+    /* pc_rhel_9_0_compat from pc_compat_6_2 */
+    { "virtio-mem", "unplugged-inaccessible", "off" },
+};
+const size_t pc_rhel_9_0_compat_len = G_N_ELEMENTS(pc_rhel_9_0_compat);
+
+GlobalProperty pc_rhel_8_5_compat[] = {
+    /* pc_rhel_8_5_compat from pc_compat_6_0 */
+    { "qemu64" "-" TYPE_X86_CPU, "family", "6" },
+    /* pc_rhel_8_5_compat from pc_compat_6_0 */
+    { "qemu64" "-" TYPE_X86_CPU, "model", "6" },
+    /* pc_rhel_8_5_compat from pc_compat_6_0 */
+    { "qemu64" "-" TYPE_X86_CPU, "stepping", "3" },
+    /* pc_rhel_8_5_compat from pc_compat_6_0 */
+    { TYPE_X86_CPU, "x-vendor-cpuid-only", "off" },
+    /* pc_rhel_8_5_compat from pc_compat_6_0 */
+    { "ICH9-LPC", ACPI_PM_PROP_ACPI_PCIHP_BRIDGE, "off" },
+
+    /* pc_rhel_8_5_compat from pc_compat_6_1 */
+    { TYPE_X86_CPU, "hv-version-id-build", "0x1bbc" },
+    /* pc_rhel_8_5_compat from pc_compat_6_1 */
+    { TYPE_X86_CPU, "hv-version-id-major", "0x0006" },
+    /* pc_rhel_8_5_compat from pc_compat_6_1 */
+    { TYPE_X86_CPU, "hv-version-id-minor", "0x0001" },
+};
+const size_t pc_rhel_8_5_compat_len = G_N_ELEMENTS(pc_rhel_8_5_compat);
+
+GlobalProperty pc_rhel_8_4_compat[] = {
+    /* pc_rhel_8_4_compat from pc_compat_5_2 */
+    { "ICH9-LPC", "x-smi-cpu-hotunplug", "off" },
+    { TYPE_X86_CPU, "kvm-asyncpf-int", "off" },
+};
+const size_t pc_rhel_8_4_compat_len = G_N_ELEMENTS(pc_rhel_8_4_compat);
+
+GlobalProperty pc_rhel_8_3_compat[] = {
+    /* pc_rhel_8_3_compat from pc_compat_5_1 */
+    { "ICH9-LPC", "x-smi-cpu-hotplug", "off" },
+};
+const size_t pc_rhel_8_3_compat_len = G_N_ELEMENTS(pc_rhel_8_3_compat);
+
+GlobalProperty pc_rhel_8_2_compat[] = {
+    /* pc_rhel_8_2_compat from pc_compat_4_2 */
+    { "mch", "smbase-smram", "off" },
+};
+const size_t pc_rhel_8_2_compat_len = G_N_ELEMENTS(pc_rhel_8_2_compat);
+
+/* pc_rhel_8_1_compat is empty since pc_4_1_compat is */
+GlobalProperty pc_rhel_8_1_compat[] = { };
+const size_t pc_rhel_8_1_compat_len = G_N_ELEMENTS(pc_rhel_8_1_compat);
+
+GlobalProperty pc_rhel_8_0_compat[] = {
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "intel-iommu", "dma-drain", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G3" "-" TYPE_X86_CPU, "rdtscp", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G4" "-" TYPE_X86_CPU, "rdtscp", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G4" "-" TYPE_X86_CPU, "npt", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G4" "-" TYPE_X86_CPU, "nrip-save", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G5" "-" TYPE_X86_CPU, "rdtscp", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G5" "-" TYPE_X86_CPU, "npt", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Opteron_G5" "-" TYPE_X86_CPU, "nrip-save", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "EPYC" "-" TYPE_X86_CPU, "npt", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "EPYC" "-" TYPE_X86_CPU, "nrip-save", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "EPYC-IBPB" "-" TYPE_X86_CPU, "npt", "off" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "EPYC-IBPB" "-" TYPE_X86_CPU, "nrip-save", "off" },
+    /** The mpx=on entries from pc_compat_3_1 are in pc_rhel_7_6_compat **/
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { "Cascadelake-Server" "-" TYPE_X86_CPU, "stepping", "5" },
+    /* pc_rhel_8_0_compat from pc_compat_3_1 */
+    { TYPE_X86_CPU, "x-intel-pt-auto-level", "off" },
+};
+const size_t pc_rhel_8_0_compat_len = G_N_ELEMENTS(pc_rhel_8_0_compat);
+
+/* Similar to PC_COMPAT_3_0 + PC_COMPAT_2_12, but:
+ * all of the 2_12 stuff was already in 7.6 from bz 1481253
+ * x-migrate-smi-count comes from PC_COMPAT_2_11 but
+ * is really tied to kernel version so keep it off on 7.x
+ * machine types irrespective of host.
+ */
+GlobalProperty pc_rhel_7_6_compat[] = {
+    /* pc_rhel_7_6_compat from pc_compat_3_0 */
+    { TYPE_X86_CPU, "x-hv-synic-kvm-only", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_3_0 */
+    { "Skylake-Server" "-" TYPE_X86_CPU, "pku", "off" },
+    /* pc_rhel_7_6_compat from pc_compat_3_0 */
+    { "Skylake-Server-IBRS" "-" TYPE_X86_CPU, "pku", "off" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { TYPE_X86_CPU, "x-migrate-smi-count", "off" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Skylake-Client" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Skylake-Client-IBRS" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Skylake-Server" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Skylake-Server-IBRS" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Cascadelake-Server" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Icelake-Client" "-" TYPE_X86_CPU, "mpx", "on" },
+    /* pc_rhel_7_6_compat from pc_compat_2_11 */
+    { "Icelake-Server" "-" TYPE_X86_CPU, "mpx", "on" },
+};
+const size_t pc_rhel_7_6_compat_len = G_N_ELEMENTS(pc_rhel_7_6_compat);
+
+/*
+ * The PC_RHEL_*_COMPAT serve the same purpose for RHEL-7 machine
+ * types as the PC_COMPAT_* do for upstream types.
+ * PC_RHEL_7_*_COMPAT apply both to i440fx and q35 types.
+ */
+
 GSIState *pc_gsi_create(qemu_irq **irqs, bool pci_enabled)
 {
     GSIState *s;
@@ -1767,6 +1922,7 @@ static void pc_machine_class_init(ObjectClass *oc, void *data)
     pcmc->kvmclock_create_always = true;
     x86mc->apic_xrupt_override = true;
     assert(!mc->get_hotplug_handler);
+    mc->async_pf_vmexit_disable = false;
     mc->get_hotplug_handler = pc_get_hotplug_handler;
     mc->hotplug_allowed = pc_hotplug_allowed;
     mc->auto_enable_numa_with_memhp = true;
@@ -1774,7 +1930,8 @@ static void pc_machine_class_init(ObjectClass *oc, void *data)
     mc->has_hotpluggable_cpus = true;
     mc->default_boot_order = "cad";
     mc->block_default_type = IF_IDE;
-    mc->max_cpus = 255;
+    /* 240: max CPU count for RHEL */
+    mc->max_cpus = 240;
     mc->reset = pc_machine_reset;
     mc->wakeup = pc_machine_wakeup;
     hc->pre_plug = pc_machine_device_pre_plug_cb;
diff --git a/hw/i386/pc_piix.c b/hw/i386/pc_piix.c
index 67107b174a..5535e1ffbf 100644
--- a/hw/i386/pc_piix.c
+++ b/hw/i386/pc_piix.c
@@ -52,6 +52,7 @@
 #include "qapi/error.h"
 #include "qemu/error-report.h"
 #include "sysemu/xen.h"
+#include "migration/migration.h"
 #ifdef CONFIG_XEN
 #include <xen/hvm/hvm_info_table.h>
 #include "hw/xen/xen_pt.h"
@@ -445,8 +446,8 @@ static void pc_i440fx_init(MachineState *machine)
     pc_init1(machine, TYPE_I440FX_PCI_DEVICE);
 }
 
-#define DEFINE_I440FX_MACHINE(major, minor) \
-    DEFINE_PC_VER_MACHINE(pc_i440fx, "pc-i440fx", pc_i440fx_init, major, minor);
+#define DEFINE_I440FX_MACHINE(major, minor, micro) \
+    DEFINE_PC_VER_MACHINE(pc_i440fx, "pc-i440fx", pc_i440fx_init, major, minor, micro);
 
 #if 0 /* Disabled for Red Hat Enterprise Linux */
 static void pc_i440fx_machine_options(MachineClass *m)
@@ -826,3 +827,100 @@ static void xenfv_machine_3_1_options(MachineClass *m)
 DEFINE_PC_MACHINE(xenfv, "xenfv-3.1", pc_xen_hvm_init,
                   xenfv_machine_3_1_options);
 #endif
+
+/* Red Hat Enterprise Linux machine types */
+
+/* Options for the latest rhel7 machine type */
+static void pc_machine_rhel7_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    m->family = "pc_piix_Y";
+    m->default_machine_opts = "firmware=bios-256k.bin,hpet=off";
+    pcmc->pci_root_uid = 0;
+    m->default_nic = "e1000";
+    m->default_display = "std";
+    m->no_parallel = 1;
+    m->numa_mem_supported = true;
+    m->auto_enable_numa_with_memdev = false;
+    machine_class_allow_dynamic_sysbus_dev(m, TYPE_RAMFB_DEVICE);
+    compat_props_add(m->compat_props, pc_rhel_compat, pc_rhel_compat_len);
+    m->alias = "pc";
+    m->is_default = 1;
+    m->smp_props.prefer_sockets = true;
+}
+
+static void pc_i440fx_rhel_machine_7_6_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    ObjectClass *oc = OBJECT_CLASS(m);
+    pc_machine_rhel7_options(m);
+    m->desc = "RHEL 7.6.0 PC (i440FX + PIIX, 1996)";
+    m->async_pf_vmexit_disable = true;
+    m->smbus_no_migration_support = true;
+
+    pcmc->pvh_enabled = false;
+    pcmc->default_cpu_version = CPU_VERSION_LEGACY;
+    pcmc->kvmclock_create_always = false;
+    /* From pc_i440fx_5_1_machine_options() */
+    pcmc->pci_root_uid = 1;
+    /* From pc_i440fx_7_0_machine_options() */
+    pcmc->enforce_amd_1tb_hole = false;
+    /* From pc_i440fx_8_0_machine_options() */
+    pcmc->default_smbios_ep_type = SMBIOS_ENTRY_POINT_TYPE_32;
+    /* From pc_i440fx_8_1_machine_options() */
+    pcmc->broken_32bit_mem_addr_check = true;
+    /* Introduced in QEMU 8.2 */
+    pcmc->default_south_bridge = TYPE_PIIX3_DEVICE;
+
+    object_class_property_add_enum(oc, "x-south-bridge", "PCSouthBridgeOption",
+                                   &PCSouthBridgeOption_lookup,
+                                   pc_get_south_bridge,
+                                   pc_set_south_bridge);
+    object_class_property_set_description(oc, "x-south-bridge",
+                                     "Use a different south bridge than PIIX3");
+
+    compat_props_add(m->compat_props, hw_compat_rhel_9_5,
+		     hw_compat_rhel_9_5_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_4,
+                     hw_compat_rhel_9_4_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_3,
+                     hw_compat_rhel_9_3_len);
+    compat_props_add(m->compat_props, pc_rhel_9_3_compat,
+                     pc_rhel_9_3_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_2,
+                     hw_compat_rhel_9_2_len);
+    compat_props_add(m->compat_props, pc_rhel_9_2_compat,
+                     pc_rhel_9_2_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_1,
+                     hw_compat_rhel_9_1_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_0,
+                     hw_compat_rhel_9_0_len);
+    compat_props_add(m->compat_props, pc_rhel_9_0_compat,
+                     pc_rhel_9_0_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_6,
+                     hw_compat_rhel_8_6_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_5,
+                     hw_compat_rhel_8_5_len);
+    compat_props_add(m->compat_props, pc_rhel_8_5_compat,
+                     pc_rhel_8_5_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_4,
+                     hw_compat_rhel_8_4_len);
+    compat_props_add(m->compat_props, pc_rhel_8_4_compat,
+                     pc_rhel_8_4_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_3,
+                     hw_compat_rhel_8_3_len);
+    compat_props_add(m->compat_props, pc_rhel_8_3_compat,
+                     pc_rhel_8_3_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_2,
+                     hw_compat_rhel_8_2_len);
+    compat_props_add(m->compat_props, pc_rhel_8_2_compat,
+                     pc_rhel_8_2_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_1, hw_compat_rhel_8_1_len);
+    compat_props_add(m->compat_props, pc_rhel_8_1_compat, pc_rhel_8_1_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_8_0, hw_compat_rhel_8_0_len);
+    compat_props_add(m->compat_props, pc_rhel_8_0_compat, pc_rhel_8_0_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_7_6, hw_compat_rhel_7_6_len);
+    compat_props_add(m->compat_props, pc_rhel_7_6_compat, pc_rhel_7_6_compat_len);
+}
+
+DEFINE_I440FX_MACHINE(7, 6, 0);
diff --git a/hw/i386/pc_q35.c b/hw/i386/pc_q35.c
index 5fb283f2df..2ca9ff3747 100644
--- a/hw/i386/pc_q35.c
+++ b/hw/i386/pc_q35.c
@@ -338,20 +338,19 @@ static void pc_q35_machine_options(MachineClass *m)
     pcmc->pci_root_uid = 0;
     pcmc->default_cpu_version = 1;
 
-    m->family = "pc_q35";
-    m->desc = "Standard PC (Q35 + ICH9, 2009)";
+    m->family = "pc_q35_Z";
     m->units_per_default_bus = 1;
-    m->default_machine_opts = "firmware=bios-256k.bin";
+    m->default_machine_opts = "firmware=bios-256k.bin,hpet=off";
     m->default_display = "std";
     m->default_nic = "e1000e";
-    m->default_kernel_irqchip_split = false;
     m->no_floppy = 1;
-    m->max_cpus = 4096;
-    m->no_parallel = !module_object_class_by_name(TYPE_ISA_PARALLEL);
+    m->max_cpus = 710;
+    m->no_parallel = 1;
     machine_class_allow_dynamic_sysbus_dev(m, TYPE_AMD_IOMMU_DEVICE);
     machine_class_allow_dynamic_sysbus_dev(m, TYPE_INTEL_IOMMU_DEVICE);
     machine_class_allow_dynamic_sysbus_dev(m, TYPE_RAMFB_DEVICE);
-    machine_class_allow_dynamic_sysbus_dev(m, TYPE_VMBUS_BRIDGE);
+    m->alias = "q35";
+    compat_props_add(m->compat_props, pc_rhel_compat, pc_rhel_compat_len);
     compat_props_add(m->compat_props,
                      pc_q35_compat_defaults, pc_q35_compat_defaults_len);
 }
@@ -670,3 +669,194 @@ static void pc_q35_machine_2_4_options(MachineClass *m)
 
 DEFINE_Q35_MACHINE(2, 4);
 #endif /* Disabled for Red Hat Enterprise Linux */
+
+/* Red Hat Enterprise Linux machine types */
+
+static void pc_q35_rhel_machine_9_4_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_machine_options(m);
+    m->desc = "RHEL-9.4.0 PC (Q35 + ICH9, 2009)";
+    pcmc->smbios_stream_product = "RHEL";
+    pcmc->smbios_stream_version = "9.4.0";
+
+    compat_props_add(m->compat_props, hw_compat_rhel_9_5,
+		     hw_compat_rhel_9_5_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(9, 4, 0);
+
+static void pc_q35_rhel_machine_9_2_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_9_4_0_options(m);
+    m->desc = "RHEL-9.2.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = "RHEL";
+    pcmc->smbios_stream_version = "9.2.0";
+
+    /* From pc_q35_8_0_machine_options() */
+    pcmc->default_smbios_ep_type = SMBIOS_ENTRY_POINT_TYPE_32;
+    /* From pc_q35_8_1_machine_options() */
+    pcmc->broken_32bit_mem_addr_check = true;
+
+    compat_props_add(m->compat_props, hw_compat_rhel_9_4,
+                     hw_compat_rhel_9_4_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_3,
+                     hw_compat_rhel_9_3_len);
+    compat_props_add(m->compat_props, pc_rhel_9_3_compat,
+                     pc_rhel_9_3_compat_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_2,
+                     hw_compat_rhel_9_2_len);
+    compat_props_add(m->compat_props, pc_rhel_9_2_compat,
+                     pc_rhel_9_2_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(9, 2, 0);
+
+static void pc_q35_rhel_machine_9_0_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_9_2_0_options(m);
+    m->desc = "RHEL-9.0.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = "RHEL";
+    pcmc->smbios_stream_version = "9.0.0";
+    pcmc->enforce_amd_1tb_hole = false;
+    compat_props_add(m->compat_props, hw_compat_rhel_9_1,
+                     hw_compat_rhel_9_1_len);
+    compat_props_add(m->compat_props, hw_compat_rhel_9_0,
+                     hw_compat_rhel_9_0_len);
+    compat_props_add(m->compat_props, pc_rhel_9_0_compat,
+                     pc_rhel_9_0_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(9, 0, 0);
+
+static void pc_q35_rhel_machine_8_6_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_9_0_0_options(m);
+    m->desc = "RHEL-8.6.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+
+    pcmc->smbios_stream_product = "RHEL-AV";
+    pcmc->smbios_stream_version = "8.6.0";
+    compat_props_add(m->compat_props, hw_compat_rhel_8_6,
+                     hw_compat_rhel_8_6_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 6, 0);
+
+static void pc_q35_rhel_machine_8_5_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_6_0_options(m);
+    m->desc = "RHEL-8.5.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = "RHEL-AV";
+    pcmc->smbios_stream_version = "8.5.0";
+    compat_props_add(m->compat_props, hw_compat_rhel_8_5,
+                     hw_compat_rhel_8_5_len);
+    compat_props_add(m->compat_props, pc_rhel_8_5_compat,
+                     pc_rhel_8_5_compat_len);
+    m->smp_props.prefer_sockets = true;
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 5, 0);
+
+static void pc_q35_rhel_machine_8_4_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_5_0_options(m);
+    m->desc = "RHEL-8.4.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = "RHEL-AV";
+    pcmc->smbios_stream_version = "8.4.0";
+    compat_props_add(m->compat_props, hw_compat_rhel_8_4,
+                     hw_compat_rhel_8_4_len);
+    compat_props_add(m->compat_props, pc_rhel_8_4_compat,
+                     pc_rhel_8_4_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 4, 0);
+
+static void pc_q35_rhel_machine_8_3_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_4_0_options(m);
+    m->desc = "RHEL-8.3.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = "RHEL-AV";
+    pcmc->smbios_stream_version = "8.3.0";
+    compat_props_add(m->compat_props, hw_compat_rhel_8_3,
+                     hw_compat_rhel_8_3_len);
+    compat_props_add(m->compat_props, pc_rhel_8_3_compat,
+                     pc_rhel_8_3_compat_len);
+    /* From pc_q35_5_1_machine_options() */
+    pcmc->kvmclock_create_always = false;
+    /* From pc_q35_5_1_machine_options() */
+    pcmc->pci_root_uid = 1;
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 3, 0);
+
+static void pc_q35_rhel_machine_8_2_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_3_0_options(m);
+    m->desc = "RHEL-8.2.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    m->numa_mem_supported = true;
+    m->auto_enable_numa_with_memdev = false;
+    pcmc->smbios_stream_product = "RHEL-AV";
+    pcmc->smbios_stream_version = "8.2.0";
+    compat_props_add(m->compat_props, hw_compat_rhel_8_2,
+                     hw_compat_rhel_8_2_len);
+    compat_props_add(m->compat_props, pc_rhel_8_2_compat,
+                     pc_rhel_8_2_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 2, 0);
+
+static void pc_q35_rhel_machine_8_1_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_2_0_options(m);
+    m->desc = "RHEL-8.1.0 PC (Q35 + ICH9, 2009)";
+    m->alias = NULL;
+    pcmc->smbios_stream_product = NULL;
+    pcmc->smbios_stream_version = NULL;
+    compat_props_add(m->compat_props, hw_compat_rhel_8_1, hw_compat_rhel_8_1_len);
+    compat_props_add(m->compat_props, pc_rhel_8_1_compat, pc_rhel_8_1_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 1, 0);
+
+static void pc_q35_rhel_machine_8_0_0_options(MachineClass *m)
+{
+    PCMachineClass *pcmc = PC_MACHINE_CLASS(m);
+    pc_q35_rhel_machine_8_1_0_options(m);
+    m->desc = "RHEL-8.0.0 PC (Q35 + ICH9, 2009)";
+    m->smbus_no_migration_support = true;
+    m->alias = NULL;
+    pcmc->pvh_enabled = false;
+    pcmc->default_cpu_version = CPU_VERSION_LEGACY;
+    compat_props_add(m->compat_props, hw_compat_rhel_8_0, hw_compat_rhel_8_0_len);
+    compat_props_add(m->compat_props, pc_rhel_8_0_compat, pc_rhel_8_0_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(8, 0, 0);
+
+static void pc_q35_rhel_machine_7_6_0_options(MachineClass *m)
+{
+    pc_q35_rhel_machine_8_0_0_options(m);
+    m->alias = NULL;
+    m->desc = "RHEL-7.6.0 PC (Q35 + ICH9, 2009)";
+    m->async_pf_vmexit_disable = true;
+    compat_props_add(m->compat_props, hw_compat_rhel_7_6, hw_compat_rhel_7_6_len);
+    compat_props_add(m->compat_props, pc_rhel_7_6_compat, pc_rhel_7_6_compat_len);
+}
+
+DEFINE_Q35_MACHINE_BUGFIX(7, 6, 0);
+
diff --git a/include/hw/boards.h b/include/hw/boards.h
index fd5a957cad..3dea5cee73 100644
--- a/include/hw/boards.h
+++ b/include/hw/boards.h
@@ -289,6 +289,8 @@ struct MachineClass {
     strList *allowed_dynamic_sysbus_devices;
     bool auto_enable_numa_with_memhp;
     bool auto_enable_numa_with_memdev;
+    /* RHEL only */
+    bool async_pf_vmexit_disable;
     bool ignore_boot_device_suffixes;
     bool smbus_no_migration_support;
     bool nvdimm_supported;
diff --git a/include/hw/i386/pc.h b/include/hw/i386/pc.h
index 8776a3c937..8e9597f40f 100644
--- a/include/hw/i386/pc.h
+++ b/include/hw/i386/pc.h
@@ -302,6 +302,39 @@ extern const size_t pc_compat_2_4_len;
 extern GlobalProperty pc_compat_2_3[];
 extern const size_t pc_compat_2_3_len;
 
+extern GlobalProperty pc_rhel_compat[];
+extern const size_t pc_rhel_compat_len;
+
+extern GlobalProperty pc_rhel_9_3_compat[];
+extern const size_t pc_rhel_9_3_compat_len;
+
+extern GlobalProperty pc_rhel_9_2_compat[];
+extern const size_t pc_rhel_9_2_compat_len;
+
+extern GlobalProperty pc_rhel_9_0_compat[];
+extern const size_t pc_rhel_9_0_compat_len;
+
+extern GlobalProperty pc_rhel_8_5_compat[];
+extern const size_t pc_rhel_8_5_compat_len;
+
+extern GlobalProperty pc_rhel_8_4_compat[];
+extern const size_t pc_rhel_8_4_compat_len;
+
+extern GlobalProperty pc_rhel_8_3_compat[];
+extern const size_t pc_rhel_8_3_compat_len;
+
+extern GlobalProperty pc_rhel_8_2_compat[];
+extern const size_t pc_rhel_8_2_compat_len;
+
+extern GlobalProperty pc_rhel_8_1_compat[];
+extern const size_t pc_rhel_8_1_compat_len;
+
+extern GlobalProperty pc_rhel_8_0_compat[];
+extern const size_t pc_rhel_8_0_compat_len;
+
+extern GlobalProperty pc_rhel_7_6_compat[];
+extern const size_t pc_rhel_7_6_compat_len;
+
 #define DEFINE_PC_MACHINE(suffix, namestr, initfn, optsfn) \
     static void pc_machine_##suffix##_class_init(ObjectClass *oc, void *data) \
     { \
diff --git a/target/i386/kvm/kvm-cpu.c b/target/i386/kvm/kvm-cpu.c
index 6bf8dcfc60..684e731cbc 100644
--- a/target/i386/kvm/kvm-cpu.c
+++ b/target/i386/kvm/kvm-cpu.c
@@ -178,6 +178,7 @@ static PropValue kvm_default_props[] = {
     { "acpi", "off" },
     { "monitor", "off" },
     { "svm", "off" },
+    { "kvm-pv-unhalt", "on" },
     { NULL, NULL },
 };
 
diff --git a/target/i386/kvm/kvm.c b/target/i386/kvm/kvm.c
index 2fa88ef1e3..2b28c18693 100644
--- a/target/i386/kvm/kvm.c
+++ b/target/i386/kvm/kvm.c
@@ -4244,6 +4244,7 @@ static int kvm_get_msrs(X86CPU *cpu)
     struct kvm_msr_entry *msrs = cpu->kvm_msr_buf->entries;
     int ret, i;
     uint64_t mtrr_top_bits;
+    MachineClass *mc = MACHINE_GET_CLASS(qdev_get_machine());
 
     kvm_msr_buf_reset(cpu);
 
@@ -4636,6 +4637,9 @@ static int kvm_get_msrs(X86CPU *cpu)
             break;
         case MSR_KVM_ASYNC_PF_EN:
             env->async_pf_en_msr = msrs[i].data;
+            if (mc->async_pf_vmexit_disable) {
+                env->async_pf_en_msr &= ~(1ULL << 2);
+            }
             break;
         case MSR_KVM_ASYNC_PF_INT:
             env->async_pf_int_msr = msrs[i].data;
diff --git a/tests/qtest/pvpanic-test.c b/tests/qtest/pvpanic-test.c
index d49d2ba931..c18f63e255 100644
--- a/tests/qtest/pvpanic-test.c
+++ b/tests/qtest/pvpanic-test.c
@@ -18,7 +18,7 @@ static void test_panic_nopause(void)
     QDict *response, *data;
     QTestState *qts;
 
-    qts = qtest_init("-device pvpanic -action panic=none");
+    qts = qtest_init("-M q35 -device pvpanic -action panic=none");
 
     val = qtest_inb(qts, 0x505);
     g_assert_cmpuint(val, ==, PVPANIC_EVENTS);
@@ -41,7 +41,8 @@ static void test_panic(void)
     QDict *response, *data;
     QTestState *qts;
 
-    qts = qtest_init("-device pvpanic -action panic=pause");
+    /* RHEL: Use q35 */
+    qts = qtest_init("-M q35 -device pvpanic -action panic=pause");
 
     val = qtest_inb(qts, 0x505);
     g_assert_cmpuint(val, ==, PVPANIC_EVENTS);
-- 
2.39.3

